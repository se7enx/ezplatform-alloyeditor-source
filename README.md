# eZ Platform AlloyEditor Source

This bundle allows editing the HTML source code of the fields managed by eZ Platform Online Editor. So it provides "disable editor" functionality from eZ Publish Legacy.

## Installation

1. Require via composer
    ```bash
    composer require contextualcode/ezplatform-alloyeditor-source
    ```

2. Add the "Content / Online Editor / Edit Source" policy to desired security roles using Admin UI:
    ![open_modal](https://gitlab.com/contextualcode/ezplatform-alloyeditor-source/raw/master/doc/images/security_policy.png)

3. Clear browser caches and enjoy!

## Usage

1. Start editing any content with Rich Text fields in eZ Platform admin UI. And click "Edit Source" button in Rich Text Online Editor toolbar:
    ![open_modal](https://gitlab.com/contextualcode/ezplatform-alloyeditor-source/raw/master/doc/images/open_modal.png)
2. A new modal will be shown, where you can edit the source code:
    ![edit_source_modal](https://gitlab.com/contextualcode/ezplatform-alloyeditor-source/raw/master/doc/images/edit_source_modal.png)

## Using CKEditor plugins in eZ Platform UI

To check the example of using the CK Editor plugin in the eZ Platform please use [1.0 branch](https://gitlab.com/contextualcode/ezplatform-alloyeditor-source/tree/1.0).