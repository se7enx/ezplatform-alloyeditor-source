<?php

namespace ContextualCode\EzPlatformAlloyEditorSource\Security;

use eZ\Bundle\EzPublishCoreBundle\DependencyInjection\Security\PolicyProvider\YamlPolicyProvider;

class AlloyEditor extends YamlPolicyProvider
{
    protected function getFiles(): array
    {
        return [__DIR__ . '/../Resources/config/policies.yaml'];
    }
}
