import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Modal from '../components/modal';
import HtmlFormatter from '../helpers/html.formatter';
import {Controlled as CodeMirror} from '../../codemirror/react-codemirror2'

export default class SourceModal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            html: new HtmlFormatter().format(this.props.html)
        };
    }

    onConfirm() {
        this.props.onConfirm(this.state.html);
    }

    render() {
        const config = {
            title: Translator.trans('modal.header', {}, 'alloy_editor_source'),
            cancelLabel: Translator.trans('modal.buttons.cancel.label', {}, 'alloy_editor_source'),
            confirmLabel: Translator.trans('modal.buttons.update.label', {}, 'alloy_editor_source'),
            onClose: this.props.onClose,
            onConfirm: this.onConfirm.bind(this),
        };
        return(
            <Modal {...config}>
                {this.renderModalContent()}
            </Modal>
        );
    }

    renderModalContent() {
        return (
            <CodeMirror
                value={this.state.html}
                options={{
                    mode: 'text/html',
                    //theme: 'material',
                    theme: 'eclipse',
                    lineNumbers: true,
                    matchTags: true,
                    matchBrackets: true,
                    lineWrapping: true,
                }}
                onBeforeChange={(editor, data, value) => {
                    this.setState({html: value});
                }}
            />
        );
    }
}

SourceModal.propTypes = {
    html: PropTypes.string.isRequired,
    onConfirm: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired,
};