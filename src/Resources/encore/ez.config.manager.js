const path = require('path');

module.exports = (eZConfig, eZConfigManager) => {
    eZConfigManager.add({
        eZConfig,
        entryName: 'ezplatform-richtext-onlineeditor-js',
        newItems: [
            path.resolve(__dirname, '../public/js/codemirror/mode/xml.js'),
            path.resolve(__dirname, '../public/js/codemirror/mode/htmlmixed.js'),
            path.resolve(__dirname, '../public/js/alloyeditor/buttons/source.js')
        ]
    });
    eZConfigManager.add({
        eZConfig,
        entryName: 'ezplatform-richtext-onlineeditor-css',
        newItems: [
            path.resolve(__dirname, '../public/css/codemirror.css'),
            path.resolve(__dirname, '../public/css/codemirror-eclipse.css'),
            path.resolve(__dirname, '../public/css/codemirror-material.css')
        ]
    });
};