<?php

namespace ContextualCode\EzPlatformAlloyEditorSource;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class EzPlatformAlloyEditorSourceBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $ezpublish = $container->getExtension('ezpublish');
        $ezpublish->addPolicyProvider(new Security\AlloyEditor());
    }
}
